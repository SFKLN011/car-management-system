from django.shortcuts import render
from .models import AutomobileV0, Customer, SalesPerson, Sales
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from django.core.exceptions import ObjectDoesNotExist
import json


from .encoders import (
    AutomobileV0Encoder,
    CustomerEncoder,
    SalesPersonEncoder,
    SalesEncoder,

)
from .models import AutomobileV0, Customer, SalesPerson, Sales


# Create your views here.
# ListSalesBySalesPersonView()


@require_http_methods(["GET", "POST"])
def api_sales_person(request):
    if request.method == "GET":
        sales_person = SalesPerson.objects.all()
        return JsonResponse(
            sales_person,
            encoder=SalesPersonEncoder,
            safe=False,
        )
    if request.method == "POST":
        try:
            content = json.loads(request.body)
            sales_person = SalesPerson.objects.create(**content)
            return JsonResponse(
                sales_person,
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the sales person"}
            )
            response.status_code = 400
            return response

@require_http_methods(["GET", "DELETE"])
def api_show_sales_persons(request,pk):
    if request.method == "GET":
        sales_person = SalesPerson.objects.get(id=pk)
        return JsonResponse(
            sales_person,
            encoder=SalesPersonEncoder,
            safe=False,
        )

    if request.method == "DELETE":
        sales_person = SalesPerson.objects.get(id=pk)
        sales_person.delete()
        return JsonResponse(
            {"message": "Sales person deleted"},
            sales_person,
            encoder=SalesPersonEncoder,
            safe=False,
        )



@require_http_methods(["GET", "POST"])
def api_customer(request):
    if request.method == "GET":
        customer = Customer.objects.all()
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )

        except:
            response = JsonResponse(
                {"message": "Could not create Customer"}
            )
            response.status_code = 400
            return response

@require_http_methods(["GET", "DELETE"])
def api_show_customers(request,pk):
    if request.method == "GET":
        customer = Customer.objects.get(id=pk)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )

    if request.method == "DELETE":
        customer = Customer.objects.get(id=pk)
        customer.delete()
        return JsonResponse(
            {"message": "Customer deleted"},
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )


@require_http_methods(["GET"])
def api_sales_detail(request, pk):
    if request.method == "GET":
        try:
            sales_record = Sales.objects.get(id=pk)
            return JsonResponse(
                sales_record,
                encoder=SalesEncoder,
                safe=False,
            )
        except Sales.DoesNotExist:
            return JsonResponse(
                {"message": "Sales record does not exist"},
                status=404,
            )


@require_http_methods(["GET", "POST"])
def api_list_sales(request):
    if request.method == "GET":
        print("aa")
        sales = Sales.objects.all()
        print(sales)
        return JsonResponse(
            sales,
            encoder=SalesEncoder,
            safe=False
        )
    elif request.method == "POST":
        content = json.loads(request.body)
        print(content)
        automobile_vin = content["automobile_vin"]
        automobile = AutomobileV0.objects.get(vin=automobile_vin)
        if automobile.available is True:
            content["automobile"] = automobile
            del content["automobile_vin"]

            customer_id = content["customer_id"]
            customer = Customer.objects.get(id=customer_id)
            content["customer"] = customer

            sales_person_id = content["sales_person_id"]
            sales_person = SalesPerson.objects.get(id=sales_person_id)
            content["sales_person"] = sales_person

            automobile.available = False
            automobile.save()

            record = Sales.objects.create(**content)
            return JsonResponse(
                {"message": "Sales record created successfully", "record": record},
                encoder=SalesEncoder,
                safe=False,
            )

        else:
            response = JsonResponse(
                {"message": "Sorry! No longer available."}
            )
            response.status_code = 400
            return response

@require_http_methods(["GET"])
def api_automobiles(request):
    if request.method == "GET":
        cars = AutomobileV0.objects.all()
        return JsonResponse(
            cars,
            encoder=AutomobileV0Encoder,
            safe=False,
        )
