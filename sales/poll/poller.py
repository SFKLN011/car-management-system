import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sales_project.settings")
django.setup()

# Import models from sales_rest, here.
# from sales_rest.models import SalesPerson

from sales_rest.models import AutomobileV0


def get_automobile():
    response = requests.get("http://inventory-api:8000/api/automobiles/")
    content = json.loads(response.content)
    for auto in content["autos"]:
        AutomobileV0.objects.update_or_create(
            import_href=auto["href"],
            defaults={
                "color": auto["color"],
                "year": auto["year"],
                "vin": auto["vin"],
                },
        )
        print(auto["href"], "updated") # this tells us what automobile was updated/created from inventory-api

def poll():
    while True:
        try:
            get_automobile()
        except Exception as e:
            print(e, file=sys.stderr)
            print("error")
        time.sleep(5) # this means that you should see the automobiles on the sales form (with the 4 unique vins)

if __name__ == "__main__":
    poll()
