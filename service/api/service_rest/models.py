from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    #Change default later?
    import_href = models.CharField(max_length=200, unique=True, null=False, default='')
    color = models.CharField(max_length=50)
    year = models.PositiveSmallIntegerField(default=0)
    vin = models.CharField(max_length=17, null=True, unique=True)
    model = models.CharField(max_length=10000, null=True)

    def __str__(self):
        return self.vin

    # def get_api_url(self):
    #     return reverse("api_automobile", kwargs={"vin": self.vin})



class Technician(models.Model):
    tech_name = models.CharField(max_length=100)
    employee_number = models.PositiveSmallIntegerField()

    def __str__(self):
        return self.tech_name


class Appointment(models.Model):

    customer_name = models.CharField(max_length=100)
    date = models.DateField(auto_now=False, auto_now_add=False, null=True)
    time = models.TimeField(auto_now=False, auto_now_add=False, null=True)
    reason = models.TextField(default='', null=True)
    customer_vin = models.CharField(max_length=17, default='', null=True)
    is_vip = models.BooleanField(default=False, null=True)

    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="appointments_auto",
        on_delete=models.CASCADE,
        null=True,

    )

    technician = models.ForeignKey(
        Technician,
        related_name="appointments_tech",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.automobile

    def is_vip_appointment(self):
        return self.automobile in AutomobileVO.objects.filter(is_vip=True)

    # def get_api_url(self):
    #     #VERY IMPORTANT TO ADD THIS REVERSE RELATIONSHIP AND UPDATE KEY/DICT
    #     return reverse("api_show_appointment", kwargs={"vin": self.automobile.vin})


    # CLASS AUTOMBOLIEVO
    # def __str__(self):
    #     return f"AutomobileVO - vin: {self.vin}"


    #CAN ADD LATER IF NEEDED!
    # model = models.ForeignKey(
    #     VehicleModel,
    #     related_name="automobiles",
    #     on_delete=models.CASCADE,
    #

    #MAYBE PUT ON TOP?
    # class VOManufacturer(models.Model):
#     name = models.CharField(max_length=100, unique=True)

# class AutoMobileVO(models.Model):
#     name = models.CharField(max_length=100)
#     picture_url = models.URLField()
