# Generated by Django 4.0.3 on 2023-03-10 01:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('service_rest', '0020_alter_appointment_customer_vin'),
    ]

    operations = [
        migrations.AlterField(
            model_name='appointment',
            name='customer_vin',
            field=models.CharField(default='', max_length=17, null=True),
        ),
    ]
