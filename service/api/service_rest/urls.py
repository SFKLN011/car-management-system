from django.urls import path

from .views import (
    api_list_appointment,
    api_show_appointment,
    api_list_technician,
    api_show_technician
)

urlpatterns = [
    path("appointments/", api_list_appointment, name="api_create_appointment"),
    path("appointments/<int:pk>/", api_show_appointment, name="api_show_appointment"),
    path("technicians/", api_list_technician, name="api_create_technician"),
    path("technicians/<int:pk>/", api_show_technician, name="api_show_technician")
]
