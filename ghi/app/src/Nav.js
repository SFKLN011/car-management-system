import { NavLink } from 'react-router-dom';


function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item dropdown">
              <NavLink className="nav-link dropdown-toggle" to="#" id="inventoryDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Inventory
              </NavLink>
              <ul className="dropdown-menu" aria-labelledby="inventoryDropdown">
                <li><NavLink className="dropdown-item" to="/create-manufacturer">CREATE Manufacturer</NavLink></li>
                <li><NavLink className="dropdown-item" to="/create-vehicle-model">CREATE Vehicle Model</NavLink></li>
                <li><NavLink className="dropdown-item" to="/create-automobile">CREATE Automobile</NavLink></li>
                <li><NavLink className="dropdown-item" to="/manufacturers">VIEW Manufacturers</NavLink></li>
                <li><NavLink className="dropdown-item" to="/vehicle-models">VIEW Vehicle Models</NavLink></li>
                <li><NavLink className="dropdown-item" to="/automobiles">VIEW Automobiles in Inventory</NavLink></li>

              </ul>
            </li>
            <li className="nav-item dropdown">
              <NavLink className="nav-link dropdown-toggle" to="#" id="salesDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Sales
              </NavLink>
              <ul className="dropdown-menu" aria-labelledby="salesDropdown">
                <li><NavLink className="dropdown-item" to="/api/sales_person">ADD Sales Person</NavLink></li>
                <li><NavLink className="dropdown-item" to="/api/customer">ADD Customer</NavLink></li>
                <li><NavLink className="dropdown-item" to="/create-sales">CREATE Sales Record</NavLink></li>
                <li><NavLink className="dropdown-item" to="/sales">VIEW All Sales</NavLink></li>
                <li><NavLink className="dropdown-item" to="/sales-history">VIEW Sales History by Sales Person</NavLink></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <NavLink className="nav-link dropdown-toggle" to="#" id="servicesDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Services
              </NavLink>
              <ul className="dropdown-menu" aria-labelledby="servicesDropdown">
                <li><NavLink className="dropdown-item" to="/enter-a-service-appointment">Enter a service appointment</NavLink></li>
                <li><NavLink className="dropdown-item" to="/enter-a-technician">Enter a technichian</NavLink></li>
                <li><NavLink className="dropdown-item" to="/service-appointments-list">Service Appointments List</NavLink></li>
                <li><NavLink className="dropdown-item" to="/service-appointments-by-VIN">Service Appointments by VIN</NavLink></li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;



// import { NavLink } from 'react-router-dom';

// function Nav() {
//   return (
//     <nav className="navbar navbar-expand-lg navbar-dark bg-success">
//       <div className="container-fluid">
//         <NavLink className="navbar-brand" to="/">CarCar</NavLink>
//         <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
//           <span className="navbar-toggler-icon"></span>
//         </button>
//         <div className="collapse navbar-collapse" id="navbarSupportedContent">
//           <ul className="navbar-nav me-auto mb-2 mb-lg-0">
//           </ul>
//         </div>
//       </div>
//     </nav>
//   )
// }

// export default Nav;
