import React, { useState, useEffect } from 'react';

function ManufacturersModelList(props) {
    const [manufacturerModels, setManufacturerModels] = useState([]);

    useEffect(() => {
        const fetchManufacturers = async () => {
            const url = "http://localhost:8100/api/manufacturers/";
            const response = await fetch(url);

            if (response.ok) {
                const data = await response.json();
                setManufacturerModels(data.manufacturers);
            }
        };
        fetchManufacturers();
    }, []);

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                </tr>
            </thead>
            <tbody>
                {manufacturerModels.map((manufacturer) => {
                    return (
                        <tr key={manufacturer.id}>
                            <td>{manufacturer.name}</td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    );
}

export default ManufacturersModelList;
