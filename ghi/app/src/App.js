import { BrowserRouter, Routes, Route } from 'react-router-dom';
import React, { useEffect, useState } from 'react'
import MainPage from './MainPage';
import Nav from './Nav';
import VehicleModelsList from './VehicleModelsList';
import VehicleModelForm from './VehicleModelForm';
import AutomobileForm from './AutomobileForm';
import AutomobileList from './AutomobileList';
import ServiceAppointmentForm from './ServiceAppointmentForm';
import CreateTechnicianForm from './CreateTechnicianForm';
import ServiceAppointmentsList from './ServiceAppointmentsList';
import ServiceAppointmentsByVIN from './ServiceAppointmentsByVIN';

import ManufacturersList from './ManufacturersList';
import ManufacturerForm from './ManufacturerForm';
import SalesPersonForm from './SalesPersonForm';
import CustomerForm from './CustomerForm';
import SalesForm from './SalesForm';
import SalesList from './SalesList';
import SalesHistory from './SalesHistory';

function App() {

  const [sales_person, setSalesperson] = useState([]);
  const fetchSalesperson = async () => {
    const url = 'http://localhost:8090/api/sales_person/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setSalesperson(data)
    }
  }

  const [customer, setCustomer] = useState([]);
  const fetchCustomer = async () => {
    const url = 'http://localhost:8090/api/customer/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setCustomer(data)
    }
  }


  const [manufacturers, setManufacturers] = useState([]);
  const fetchManufacturers = async () => {
    const url = 'http://localhost:8100/api/manufacturers/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setManufacturers(data.manufacturers)
    }
  }

  const [models, setModels] = useState([]);
  const fetchModels = async () => {
    const url = 'http://localhost:8100/api/models/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setModels(data.models)
    }
  }

  const [sales, setSales] = useState([]);
  const fetchSales = async () => {
    const url = 'http://localhost:8090/api/sales/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();

      setSales(data)
    }
  }

  const [automobiles, setAutomobile] = useState([]);
  const fetchAutomobiles = async () => {
    const url = 'http://localhost:8100/api/automobiles/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setAutomobile(data)
    }
  }

  const [automobilesv0, setAutomobilev0] = useState([]);
  const fetchAutomobilev0 = async () => {
    const url = 'http://localhost:8090/api/automobiles/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setAutomobilev0(data);
    }
  }


  useEffect(() => {
    fetchManufacturers();
    fetchModels();
    fetchAutomobiles();
    fetchAutomobilev0();
    fetchSalesperson();
    fetchCustomer();
    fetchSales();
  }, []);



  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/vehicle-models" element={<VehicleModelsList />} />
          <Route path="/create-vehicle-model" element={<VehicleModelForm />} />
          <Route path="/manufacturers" element={<ManufacturersList />} />
          <Route path="/create-manufacturer" element={<ManufacturerForm />} />
          <Route path="/automobiles" element={<AutomobileList />} />
          <Route path="/create-automobile" element={<AutomobileForm />} />
          <Route path="/sales" element={<SalesList sales_person={sales_person} sales={sales} />} />
          <Route path="/sales-history" element={<SalesHistory sales_person={sales_person} sales={sales} />} />
          <Route path="/create-sales" element={<SalesForm fetchAutomobilev0={fetchAutomobilev0} automobiles={automobilesv0} sales_person={sales_person} customer={customer} />} />
          <Route path="/api/sales_person" element={<SalesPersonForm fetchSalesperson={fetchSalesperson} />} />
          <Route path="/api/customer" element={<CustomerForm fetchCustomer={fetchCustomer} />} />
          <Route path="/enter-a-service-appointment" element={<ServiceAppointmentForm />} />
          <Route path="/enter-a-technician" element={<CreateTechnicianForm />} />
          <Route path="/service-appointments-list" element={<ServiceAppointmentsList />} />
          <Route path="/service-appointments-by-VIN" element={<ServiceAppointmentsByVIN />} />

        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
