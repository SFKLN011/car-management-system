import { useState, useEffect } from 'react';

function SalesForm(props) {
    const [automobile, setAutomobile] = useState('');
    const handleAutomobileChange = (event) => {
        const value = event.target.value;
        setAutomobile(value);
    }

    const [sales_person, setSalesPerson] = useState('');
    const handleSalesPersonChange = (event) => {
        const value = event.target.value;
        setSalesPerson(value);
    }

    const [customer, setCustomer] = useState('');
    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setCustomer(value);
    }

    const [sale_price, setPrice] = useState([]);
    const handlePriceChange = (event) => {
        const value = event.target.value;
        setPrice(value);
    }


    const handleFormSubmit = async (event) => {
        event.preventDefault();

        const data = {
            automobile_vin: automobile,
            sales_person_id: sales_person,
            customer_id: customer,
            sale_price: sale_price,
        };

        const url = "http://localhost:8090/api/sales/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            }
        };

        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            const newSales = await response.json();
            setAutomobile("");
            setSalesPerson("");
            setCustomer("");
            setPrice("");
            await props.fetchAutomobilev0();

            // Show "View all Sales" button
            const viewAllBtn = document.createElement('a');
            viewAllBtn.href = 'http://localhost:3000/sales';
            viewAllBtn.className = 'btn btn-primary mt-3';
            viewAllBtn.textContent = 'View all sales';
            document.getElementById('create-sales-form').appendChild(viewAllBtn);
        } else {
            console.log('Error creating new sale');
        }
    };


    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a sales record</h1>
                    <form id="create-sales-form" onSubmit={handleFormSubmit}>
                        <div className="mb-3">
                            <select
                                required
                                name="manufacturer"
                                id="automobile"
                                className="form-select"
                                value={automobile}
                                onChange={(event) => setAutomobile(event.target.value)}
                            >
                                <option value="">
                                    Choose a automobile
                                </option>
                                {(props.automobiles || []).map(automobile => {

                                    if (automobile.available === true)
                                        return (
                                            <option key={automobile.vin} value={automobile.vin}>
                                                {automobile.vin}
                                            </option>
                                        );

                                })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <select
                                required
                                name="sales_person"
                                id="sales_person"
                                className="form-select"
                                value={sales_person}
                                onChange={(event) => setSalesPerson(event.target.value)}
                            >
                                <option value="">
                                    Choose a Sales Person
                                </option>
                                {(props.sales_person || []).map(sales_person => {
                                    return (
                                        <option key={sales_person.id} value={sales_person.id}>
                                            {sales_person.name}
                                        </option>
                                    );

                                })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <select
                                required
                                name="customer"
                                id="customer"
                                className="form-select"
                                value={customer}
                                onChange={(event) => setCustomer(event.target.value)}
                            >
                                <option value="">
                                    Choose a Customer
                                </option>
                                {(props.customer || []).map(customer => {
                                    return (
                                        <option key={customer.id} value={customer.id}>
                                            {customer.name}
                                        </option>
                                    );

                                })}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                placeholder="Price"
                                required
                                type="number"
                                name="sale_price"
                                id="sale_price"
                                className="form-control"
                                value={sale_price}
                                onChange={(event) => setPrice(event.target.value)}
                            />
                            <label htmlFor="name">Price</label>
                        </div>
                        <button className="btn btn-primary" type="submit">
                            Create
                        </button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default SalesForm;
