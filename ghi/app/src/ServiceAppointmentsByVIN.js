import React, { useState } from 'react';

function ServiceAppointmentsList() {
    const [appointments, setAppointments] = useState([]);
    const [customer_vin, setVinSearchTerm] = useState("");

    const fetchServiceAppointments = () => {
        fetch(`http://localhost:8080/api/appointments/?customer_vin=${customer_vin}`)
            .then(response => response.json())
            .then(data => {
                const filteredAppointments = data.appointment.filter(appointment => appointment.customer_vin === customer_vin);
                setAppointments(filteredAppointments);
            })
            .catch((error) => {
                console.error('Error fetching appointments:', error);
            });
    };

    const handleSearch = () => {
        if (customer_vin !== "") {
            fetchServiceAppointments();
        } else {
            setAppointments([]);
        }
    };

    return (
        <div>
            <input type="text" placeholder="Search by VIN" value={customer_vin} onChange={(event) => setVinSearchTerm(event.target.value)} />
            <button onClick={handleSearch}>Submit</button>

            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Customer Name</th>
                        <th>Appointment Date</th>
                        <th>Appointment Time</th>
                        <th>Assigned Technican</th>
                        <th>Appontment Reason</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments.map((appointment) => (
                        <tr key={appointment.customer_vin}>
                            <td>{appointment.customer_vin}</td>
                            <td>{appointment.customer_name}</td>
                            <td>{appointment.date}</td>
                            <td>{appointment.time}</td>
                            <td>{appointment.technician}</td>
                            <td>{appointment.reason}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
}

export default ServiceAppointmentsList;
