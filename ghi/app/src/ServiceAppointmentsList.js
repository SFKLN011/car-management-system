import React, { useState, useEffect } from 'react';


function ServiceAppointmentsList() {
    const [appointments, setAppointments] = useState([]);


    const handleDelete = async (id) => {
        try {
            await fetch(`http://localhost:8080/api/appointments/${id}/`, {
                method: 'DELETE',
            });
            setAppointments(appointments.filter(a => a.id !== id));
            console.log(`Deleted appointment with id ${id}`);
        } catch (error) {
            console.error('Error deleting appointment:', error);
        }
    };




    useEffect(() => {
        fetch('http://localhost:8080/api/appointments/')
            .then(response => response.json())
            .then(data => setAppointments(data.appointment));
    }, []);

    return (
        <div>

            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>VIP Status</th>
                        <th>Customer Name</th>
                        <th>Appointment Date</th>
                        <th>Appointment Time</th>
                        <th>Assigned Technican</th>
                        <th>Appontment Reason</th>
                        <th>Cancel Appointment</th>
                        <th>Complete Appointment</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments.map((appointment) => (
                        <tr key={appointment.customer_vin}>
                            <td>{appointment.customer_vin}</td>
                            <td>{appointment.is_vip ? 'Yes' : 'No'}</td>
                            <td>{appointment.customer_name}</td>
                            <td>{appointment.date}</td>
                            <td>{appointment.time}</td>
                            <td>{appointment.technician}</td>
                            <td>{appointment.reason}</td>

                            <td>
                                {!appointment.isFinished && (
                                    <button
                                        className="btn btn-danger"
                                        onClick={() => handleDelete(appointment.id)}
                                    >
                                        Cancel
                                    </button>
                                )}
                            </td>
                            <td>
                                {!appointment.isFinished && (
                                    <button
                                        className="btn btn-success"
                                        onClick={() => handleDelete(appointment.id)}
                                    >
                                        Finish

                                    </button>
                                )}
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );

}

export default ServiceAppointmentsList;
